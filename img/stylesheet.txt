.sprite {
    background-image: url(spritesheet.png);
    background-repeat: no-repeat;
    display: block;
}

.sprite-G_ {
    width: 25px;
    height: 16px;
    background-position: -5px -5px;
}

.sprite-css_sprites {
    width: 88px;
    height: 80px;
    background-position: -5px -31px;
}

.sprite-facebook1 {
    width: 10px;
    height: 20px;
    background-position: -103px -5px;
}

.sprite-ic_caledar {
    width: 20px;
    height: 20px;
    background-position: -103px -35px;
}

.sprite-ic_caledar-_1_ {
    width: 19px;
    height: 19px;
    background-position: -103px -65px;
}

.sprite-ic_forum {
    width: 28px;
    height: 20px;
    background-position: -123px -5px;
}

.sprite-ic_forum-_1_ {
    width: 28px;
    height: 20px;
    background-position: -133px -35px;
}

.sprite-ic_journey {
    width: 20px;
    height: 20px;
    background-position: -132px -65px;
}

.sprite-ic_journey-_1_ {
    width: 20px;
    height: 20px;
    background-position: -103px -95px;
}
